//
//  ViewController.swift
//  MyVoiceRecognition
//
//  Created by Samat Murzaliev on 05.04.2023.
//

import InstantSearchVoiceOverlay
import UIKit

class ViewController: UIViewController, VoiceOverlayDelegate {
    
    func recording(text: String?, final: Bool?, error: Error?) {
        
    }
    
    
    let voiceOverlay = VoiceOverlayController()
    
    @IBOutlet var myButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        myButton.backgroundColor = .systemRed
        myButton.setTitleColor(.white, for: .normal)
    }
    
    @IBAction func didTapButton() {
        voiceOverlay.delegate = self
        voiceOverlay.settings.autoStart = false
        voiceOverlay.settings.autoStop = true
        voiceOverlay.settings.autoStopTimeout = 3
        voiceOverlay.start(on: self, textHandler: { text, final, _ in
            if final {
                print("Final text: \(text)")
            }
        }) { error in
            
        }
    }


}

